let express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB Connection
mongoose.connect("mongodb+srv://admin_zandro:All!sw3ll051591@cluster0.6i70n.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


// Set Notification for the connection success or failure
// Connection to the data base
let db = mongoose.connection;

// If the connection error occures, output a message in console
db.on("error", console.error.bind(console, "connection error"));

// Connection if the connection is successful output a message is the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Create a Task Schema

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		// default values are the predefined values for a field
		default : "pending"
	}
})

const userSchema = new mongoose.Schema({
	name : String,
	password : String,
	status : {
		type : String,
		// default values are the predefined values for a field
		default : "pending"
	}
})

// Create a Models
// Server  > Schema > Database > Collection (MongoDB)
const Task = mongoose.model("Task", taskSchema);

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (error, result) => {
		// If a document was found and the document's name matches the informations sent via the client/postman
		if(result != null && result.name == req.body.name){
			// Returns a message to the client/postman
			return res.send("Duplicate task found")
		}
		// If no document found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if (saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document
				else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// If an error occured
		if(err){
			// Will print any errors found in the console
			return console.log (err);
		}
		// if no errors found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// ACTIVITY

// Create a POST route (new user)
app.post("/signup", (req, res) => {
	User.findOne({name : req.body.name, password : req.body.password}, (error, result) => {
		
		if(result != null && result.name == req.body.name){
			// for same user details
			return res.send("Duplicate user found")
		}
		else{
			// new task and save it to the database
			let newUser = new User({
				name : req.body.name,
				password : req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				// for nor error in saving details
				if (saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document
				else{
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`));